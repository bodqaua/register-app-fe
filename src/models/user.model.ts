export interface UserModel {
    step1?: {
        firstName: string;
        lastName: string;
        email: string;
    };
    step2?: {
        arrivalDate: Date;
        companyName: string;
        companyPosition: string;
        role: string;
        sex: string;
        birthdate: Date;
        country: string;
        password?: string;
        deleted?: boolean;
        deletedAt?: Date;
    }
}