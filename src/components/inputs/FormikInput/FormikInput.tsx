import React from "react";
import {Field} from "formik";
import {TextField} from "@material-ui/core";

import "./formikInput.scss";

interface IProps {
    name: string;
    showError?: boolean;
    label: string;
    variant?: string;
}

export const FormikInput = ({name, showError = true, label, variant}: IProps) => {
    return (
        <div className="formikInput">
            <Field name={name} className={""}>
                {({field, meta}: any) => (
                    <>
                        <div>
                            <TextField label={label}
                                       {...field}
                                       variant={variant || "outlined"}/>
                        </div>
                        <div className={`error ${showError && meta.touched && meta.error ? 'visible' : ''}`}>
                            {meta.error} &nbsp;
                        </div>
                    </>
                )}
            </Field>
        </div>

    )
}
