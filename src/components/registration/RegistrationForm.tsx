import React, {useState} from "react";
import {inject, observer} from 'mobx-react';
import Step1 from "./step1/Step-1";
import Step2 from "./Step-2";
import Step3 from "./Step-3";

import './registration.scss';

const RegistrationForm = ({}) => {
    const [step, setStep] = useState(1);

    const nextStep = (stepToGo: number) => {
        setStep(stepToGo);
    }
    return (
        <div className="registration">
            <div className="step-wrapper">
                <div className="image-block">
                    <img src={`/images/register-${step}.jpg`} alt="register-form-step-1"/>

                </div>
                <div className="form-block">
                    {step === 1 && <Step1 emitStep={nextStep}/>}
                    {step === 2 && <Step2/>}
                    {step === 3 && <Step3/>}
                </div>
            </div>
        </div>
    )
}

export default inject()(observer(RegistrationForm));