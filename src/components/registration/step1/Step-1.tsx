import React from "react";
import {inject, observer} from 'mobx-react';
import {Button} from "@material-ui/core";
import {Form, Formik} from 'formik';
import * as Yup from 'yup';

import "./step-1.scss";
import {FormikInput} from "../../inputs/FormikInput/FormikInput";

interface IFirstStepData {
    firstName: string;
    lastName: string;
    email: string;
}

interface IProps {
    emitStep: (step: number) => void;
}

const initialValues: IFirstStepData = {
    firstName: '',
    lastName: '',
    email: ''
}


const Step1 = ({emitStep}: IProps) => {
    const schema = Yup.object().shape({
        firstName: Yup.string().required('This field is required'),
        lastName: Yup.string().required('This field is required'),
        email: Yup.string().required('This field is required').email("Invalid email"),
    })

    const submit = (values: IFirstStepData) => {
        console.log(values);
        emitStep(2);
    }

    return (
        <div className="step-body">
            <h2>Registration info</h2>
            <Formik initialValues={initialValues}
                    validationSchema={schema}
                    onSubmit={submit}>
                {props => (
                    <Form>
                        <FormikInput name="firstName" label={"First Name"}/>
                        <FormikInput name="lastName" label={"Last Name"}/>
                        <FormikInput name="email" label={"Email"}/>
                        <div className="buttons-block">
                            <Button type={"submit"} variant="contained" color="primary">Next</Button>
                        </div>
                    </Form>
                )}
            </Formik>

        </div>
    )
}

export default inject()(observer(Step1));