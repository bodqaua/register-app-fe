import React from 'react';
import logo from './logo.svg';
import './App.css';
import {BrowserRouter as Router, Switch, Route, Redirect} from "react-router-dom";
import RegistrationForm from "./components/registration/RegistrationForm";

function App() {
  return (
    <div className="App">
      <Router>
        <Switch>
            <Route path={"/register"}>
                <RegistrationForm />
            </Route>
            <Redirect to={"/register"} />
        </Switch>
      </Router>
    </div>
  );
}

export default App;
